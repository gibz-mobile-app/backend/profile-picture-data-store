using System.Net.Http.Headers;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Logging;
using ProfilePictureDataStore.Authorization;
using ProfilePictureDataStore.Models;
using ProfilePictureDataStore.Services;
using ProfilePictureDataStore.Utilities;

var builder = WebApplication.CreateBuilder(args);

IdentityModelEventSource.ShowPII = true;
IdentityModelEventSource.LogCompleteSecurityArtifact = true;

// Define default CORS policy
builder.Services.AddCors(options =>
{
    options.AddDefaultPolicy(policy =>
    {
        policy.WithOrigins("http://localhost:5173")
              .AllowAnyMethod()
              .AllowAnyHeader()
              .AllowCredentials();
    });
});

// Add configuration
builder.Services.Configure<GoogleCloudStorageConfigOptions>(builder.Configuration.GetSection("GoogleCloudStorage"));
builder.Services.Configure<EmailNotificationConfiguration>(builder.Configuration.GetSection("EmailNotification"));
builder.Services.Configure<CenterboardExportConfiguration>(builder.Configuration.GetSection("CenterboardExport"));

// Configure database connection
var serverVersion = new MySqlServerVersion(new Version(8, 0, 36));
var connectionString = builder.Configuration.GetConnectionString("ProfilePictureMySql");
builder.Services.AddDbContext<ProfilePictureDbContext>(options => options.UseMySql(connectionString, serverVersion));

// Add auth
builder.Services.AddAuthentication()
                .AddJwtBearer();

// Add services to the container.
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddScoped<IEmailNotificationService, RemoteInternalEmailNotificationService>();
builder.Services.AddScoped<IProfilePictureRequestService, ProfilePictureRequestService>();
builder.Services.AddScoped<IReviewService, ReviewService>();
builder.Services.AddScoped<ICloudStorageService, GoogleCloudStorageService>();
builder.Services.AddScoped<IPersonQueryService, CenterboardApiPersonQueryService>();
builder.Services.AddScoped<IProfilePictureExportService, CenterboardApiExportService>();

builder.Services.AddHttpClient("centerboard", client =>
{
    client.BaseAddress = new Uri(builder.Configuration.GetValue<string>("CenterboardExport:BaseURL")!);

    var username = builder.Configuration.GetValue<string>("CenterboardExport:BasicAuthUsername");
    var password = builder.Configuration.GetValue<string>("CenterboardExport:BasicAuthPassword");
    var credentials = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{username}:{password}"));
    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", credentials);
});

// Add authorization policy
builder.Services.AddAuthorizationBuilder()
    .AddPolicy(ReviewerRequirement.PolicyName, policy => policy.AddRequirements(new ReviewerRequirement()))
    .AddPolicy("PersonCreationScope", policy => policy.RequireClaim("scope", "profilePicture.createPerson"));

// Add controllers
builder.Services.AddControllers();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();

    app.UseCors();
}

app.MapControllers();

app.Run();
