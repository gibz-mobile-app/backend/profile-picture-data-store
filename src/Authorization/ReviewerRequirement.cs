using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;

namespace ProfilePictureDataStore.Authorization;
public class ReviewerRequirement : IAuthorizationHandler, IAuthorizationRequirement
{
    public const string PolicyName = "ValidProfilePictureReviewer";
    public string ExpectedRole => "profilePictureReviewer";

    public Task HandleAsync(AuthorizationHandlerContext context)
    {
        var userIdClaim = context.User.FindFirst(ClaimTypes.NameIdentifier);
        if (userIdClaim is null)
        {
            // Indicate missing claim for user id (sub)
            return Task.CompletedTask;
        }

        if (!Guid.TryParse(userIdClaim.Value, out _))
        {
            // The value of the sub claim is no valid guid.
            return Task.CompletedTask;
        }

        var roleClaims = context.User.FindAll(ClaimTypes.Role);
        if (roleClaims is null || !roleClaims.Any())
        {
            // Indicate missing role claim.
            return Task.CompletedTask;
        }

        // Check whether the role claims value contains the expected role
        var roles = roleClaims.Select(claim => claim.Value);
        if (roles.Contains(ExpectedRole))
        {
            context.Succeed(this);
        }

        return Task.CompletedTask;
    }
}