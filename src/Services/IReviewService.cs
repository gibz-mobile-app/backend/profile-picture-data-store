using ProfilePictureDataStore.Models.DataTransferObjects.Request;
using ProfilePictureDataStore.Models.Entities;

namespace ProfilePictureDataStore.Services;

public interface IReviewService
{
    public Task<IEnumerable<ProfilePictureReview>> GenerateReviewEntitiesAsync(IEnumerable<CreateReviewRequestDTO> reviewDTOs, Guid reviewerId, string? reviewerUserName);

    public Task<IEnumerable<ProfilePictureRequest>> GenerateFollowUpRequestsAsync(IEnumerable<CreateReviewRequestDTO> reviewDTOs);
}