namespace ProfilePictureDataStore.Services;

public interface ICloudStorageService
{
    public Task<string> GetSignedUrlAsync(string fileName, int timeOutInSeconds = 30);
    public Task<string> UploadFileAsync(IFormFile file, string fileName);
    public Task<string> GetBase64EncodedFile(string fileName);
    public Task DeleteFileAsync(string fileName);
}