using Microsoft.EntityFrameworkCore;
using ProfilePictureDataStore.Models;
using ProfilePictureDataStore.Models.DataTransferObjects.Request;
using ProfilePictureDataStore.Models.Entities;

namespace ProfilePictureDataStore.Services;

public class ReviewService(ProfilePictureDbContext dbContext, IProfilePictureRequestService profilePictureRequestService) : IReviewService
{
    private readonly ProfilePictureDbContext _dbContext = dbContext;
    private readonly IProfilePictureRequestService _profilePictureRequestService = profilePictureRequestService;

    public async Task<IEnumerable<ProfilePictureReview>> GenerateReviewEntitiesAsync(IEnumerable<CreateReviewRequestDTO> reviewDTOs, Guid reviewerId, string? reviewerUserName)
    {
        var rejectReasons = await _dbContext.RejectReasons.ToListAsync();

        return reviewDTOs.Select(review => new ProfilePictureReview
        {
            ReviewerId = reviewerId,
            ReviewerUserName = reviewerUserName,
            ProfilePictureId = review.ProfilePictureId,
            ReviewState = review.ReviewState,
            ReviewDateTime = DateTime.Now,
            RejectReason = rejectReasons.FirstOrDefault(reason => reason.Id == review.RejectReasonId),
        });
    }

    public async Task<IEnumerable<ProfilePictureRequest>> GenerateFollowUpRequestsAsync(IEnumerable<CreateReviewRequestDTO> reviewDTOs)
    {
        // Get the IDs of profile pictures which were rejected
        var profilePictureIds = reviewDTOs.Where(reviewDTO => reviewDTO.ReviewState == EReviewState.Rejected)
                                          .Select(reviewDTO => reviewDTO.ProfilePictureId);

        if (!profilePictureIds.Any())
        {
            return [];
        }

        // Load request entities for profile pictures
        var requestsToRenew = await _dbContext.ProfilePictureRequests
            .Include(request => request.Person)
            .Include(request => request.ProfilePicture)
            .ThenInclude(profilePicture => profilePicture!.Review)
            .ThenInclude(review => review.RejectReason)
            .Where(request => request.ProfilePicture != null && profilePictureIds.Contains(request.ProfilePicture.Id))
            .ToListAsync();

        // Generate follow-up request entities
        return _profilePictureRequestService.GenerateFollowUpRequests(requestsToRenew);
    }
}