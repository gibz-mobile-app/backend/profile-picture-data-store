using ProfilePictureDataStore.Models.DataTransferObjects.Request;
using ProfilePictureDataStore.Models.Entities;

namespace ProfilePictureDataStore.Services;

public interface IEmailNotificationService
{
    public CreateEmailNotificationRequest GetEmailNotificationRequest(ProfilePictureRequest profilePictureRequest, bool sendImmediately = false);
    public Task<CreateEmailNotificationRequest> GetEmailNotificationRequestAsync(ProfilePicture profilePicture);

    public Task SendEmailNotificationRequestsAsync(IEnumerable<CreateEmailNotificationRequest> notificationRequests);
}