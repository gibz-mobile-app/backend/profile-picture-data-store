using Google.Apis.Auth.OAuth2;
using Google.Cloud.Storage.V1;
using Microsoft.Extensions.Options;
using ProfilePictureDataStore.Utilities;

namespace ProfilePictureDataStore.Services;

public class GoogleCloudStorageService : ICloudStorageService
{

    private readonly GoogleCloudStorageConfigOptions _options;
    private readonly GoogleCredential _googleCredentials;

    public GoogleCloudStorageService(IOptions<GoogleCloudStorageConfigOptions> options)
    {
        _options = options.Value;
        try
        {
            var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            if (environment == Environments.Production)
            {
                // Store the json file in Secrets.
                _googleCredentials = GoogleCredential.FromJson(_options.AuthFileLocation);
            }
            else
            {
                _googleCredentials = GoogleCredential.FromFile(_options.AuthFileLocation);
            }
        }
        catch (Exception)
        {
            throw;
        }
    }

    public async Task<string> GetSignedUrlAsync(string fileName, int timeOutInSeconds = 30)
    {
        try
        {
            var urlSigner = UrlSigner.FromCredential(_googleCredentials);
            var signedUrl = await urlSigner.SignAsync(_options.BucketName, fileName, TimeSpan.FromSeconds(timeOutInSeconds));
            return signedUrl.ToString();
        }
        catch (Exception)
        {
            throw;
        }
    }

    public async Task<string> UploadFileAsync(IFormFile file, string fileName)
    {
        try
        {
            // Copy uploaded file to memory stream
            using var memoryStream = new MemoryStream();
            await file.CopyToAsync(memoryStream);

            // Create storage client with google credentials
            using var storageClient = StorageClient.Create(_googleCredentials);

            // Upload file
            var uploadedFile = await storageClient.UploadObjectAsync(_options.BucketName, fileName, file.ContentType, memoryStream);
            return uploadedFile.MediaLink;
        }
        catch (Exception)
        {
            throw;
        }
    }

    public async Task<string> GetBase64EncodedFile(string fileName)
    {
        // Create storage client with google credentials
        using var storageClient = StorageClient.Create(_googleCredentials);

        // Create an empty memory stream for the storage client to download the file into
        using var memoryStream = new MemoryStream();

        // Actually download the file into the memoryStream.
        // The return value contains some meta data of the downloaded file and is not relevant here.
        _ = await storageClient.DownloadObjectAsync(_options.BucketName, fileName, memoryStream);

        return Convert.ToBase64String(memoryStream.ToArray());
    }

    public async Task DeleteFileAsync(string fileName)
    {
        // Create storage client with google credentials
        using var storageClient = StorageClient.Create(_googleCredentials);

        // Delete the object in the bucket.
        await storageClient.DeleteObjectAsync(_options.BucketName, fileName);
    }
}