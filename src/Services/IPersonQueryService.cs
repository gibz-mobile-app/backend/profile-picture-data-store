using ProfilePictureDataStore.Models.DataTransferObjects.Request;

namespace ProfilePictureDataStore.Services;

public interface IPersonQueryService
{
    public Task<List<CreatePersonRequest>?>  FindPersonsAsync(string searchProperty, string searchValue);
}
