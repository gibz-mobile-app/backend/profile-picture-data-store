using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Web;
using Microsoft.Extensions.Options;
using ProfilePictureDataStore.Models.DataTransferObjects.Request;
using ProfilePictureDataStore.Models.DataTransferObjects.Response;
using ProfilePictureDataStore.Models.Entities;
using ProfilePictureDataStore.Utilities;

namespace ProfilePictureDataStore.Services;

public class CenterboardApiPersonQueryService : IPersonQueryService, IDisposable
{

    private readonly CenterboardExportConfiguration _centerboardExportConfiguration;
    private readonly HttpClient _httpClient;
    private readonly string[] _personEndpoints;

    public CenterboardApiPersonQueryService(
        IHttpClientFactory httpClientFactory,
        IOptions<CenterboardExportConfiguration> centerboardExportConfig
    )
    {
        _centerboardExportConfiguration = centerboardExportConfig.Value;

        _httpClient = httpClientFactory.CreateClient("centerboard");

        _personEndpoints = [
            _centerboardExportConfiguration.StudentsEndpoint,
            _centerboardExportConfiguration.TeachersEndpoint,
            _centerboardExportConfiguration.EmployeesEndpoint
        ];
    }

    public async Task<List<CreatePersonRequest>?> FindPersonsAsync(string searchProperty, string searchValue)
    {
        // Generate the search query string to be appended as GET parameter
        var query = GetSearchQuery(searchProperty, searchValue);

        var endpointIndex = 0;

        do
        {
            // Prepare and send http request
            var endpoint = _personEndpoints[endpointIndex];
            var path = endpoint.Remove(endpoint.Length - 1) + "?" + query;
            var centerboardPersons = await _httpClient.GetFromJsonAsync<List<CenterboardPersonResponseDTO>>(path);

            // Check if at least 1 person was found
            if (centerboardPersons is not null && centerboardPersons.Count > 0)
            {
                // Transform the search result into the final form
                var persons = centerboardPersons.Select(centerboardPerson =>
                {
                    var cohort = GetCohort(centerboardPerson.RegularClasses);
                    return new CreatePersonRequest
                    (
                        centerboardPerson.IdNr,
                        centerboardPerson.UserName,
                        centerboardPerson.FirstName,
                        centerboardPerson.LastName,
                        cohort?.UniqueName ?? "",
                        cohort?.DisplayName ?? ""
                    );
                });
                return persons.ToList();
            }

            // If no person was found, try to find it at another endpoint
            // (for a different type of persons)
            endpointIndex++;
        }
        while (endpointIndex < _personEndpoints.Length);

        return null;
    }

    private static NameValueCollection GetSearchQuery(string searchProperty, string searchValue)
    {
        var filter = $"{searchProperty}=={searchValue}";
        return HttpUtility.ParseQueryString($"?filter={filter}");
    }

    private static Cohort? GetCohort(CenterboardClassResponseDTO[]? centerboardClasses)
    {
        if (centerboardClasses is null || centerboardClasses.Length == 0)
        {
            return null;
        }

        var singleDigitPattern = new Regex(@"(?<!\d)\d(?!\d)");

        var currentYear = DateTime.Now.Year;
        var currentMonth = DateTime.Now.Month;

        foreach (var centerboardClass in centerboardClasses)
        {
            var token = centerboardClass.Token;
            if (!token.StartsWith("bm", StringComparison.CurrentCultureIgnoreCase))
            {
                var match = singleDigitPattern.Match(token);
                if (match.Captures.Count == 1)
                {
                    if (int.TryParse(match.Captures[0].Value, out int yearNumber))
                    {
                        yearNumber -= currentMonth >= 7 ? 1 : 0;
                        var replacementYear = currentYear - yearNumber;
                        var classUniqueName = singleDigitPattern.Replace(token, replacementYear.ToString());

                        return new Cohort { UniqueName = classUniqueName, DisplayName = token };
                    }
                }
            }
        }

        return null;
    }

    public void Dispose()
    {
        _httpClient.Dispose();
    }
}
