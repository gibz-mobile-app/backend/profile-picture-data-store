using System.Text;
using System.Web;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using ProfilePictureDataStore.Models;
using ProfilePictureDataStore.Models.DataTransferObjects.Response;
using ProfilePictureDataStore.Models.Entities;
using ProfilePictureDataStore.Utilities;

namespace ProfilePictureDataStore.Services;
public class CenterboardApiExportService : IProfilePictureExportService, IDisposable
{
    private readonly ProfilePictureDbContext _dbContext;
    private readonly ICloudStorageService _storageService;
    private readonly CenterboardExportConfiguration _centerboardExportConfiguration;
    private readonly HttpClient _httpClient;
    private readonly string[] _personEndpoints;

    public CenterboardApiExportService(ProfilePictureDbContext dbContext,
                                       ICloudStorageService storageService,
                                       IHttpClientFactory httpClientFactory,
                                       IOptions<CenterboardExportConfiguration> centerboardExportConfig)
    {
        _dbContext = dbContext;
        _storageService = storageService;
        _centerboardExportConfiguration = centerboardExportConfig.Value;

        _httpClient = httpClientFactory.CreateClient("centerboard");

        _personEndpoints = [
            _centerboardExportConfiguration.StudentsEndpoint,
            _centerboardExportConfiguration.TeachersEndpoint,
            _centerboardExportConfiguration.EmployeesEndpoint
        ];
    }

    public async Task<IEnumerable<ProfilePicture>> ProcessPendingExportsAsync()
    {
        // Get pending exports
        var pendingExports = await _dbContext.PendingExports
            .Take(_centerboardExportConfiguration.BatchSize)
            .Include(export => export.ProfilePicture)
            .ThenInclude(profilePicture => profilePicture.Person)
            .Include(export => export.ProfilePicture)
            .ThenInclude(profilePicture => profilePicture.ProfilePictureRequest)
            .ToListAsync();

        List<ProfilePicture> exportedProfilePictures = [];

        if (pendingExports.Count > 0)
        {
            var persons = pendingExports.Select(export => export.ProfilePicture.Person);
            var centerboardIds = await GetPersonIds(persons);

            for (int i = 0; i < _personEndpoints.Length; i++)
            {
                var endpointUrl = _personEndpoints[i];
                foreach (var idPair in centerboardIds[i])
                {
                    var export = pendingExports.FirstOrDefault(export => export.ProfilePicture.Person.PublicIdentifier == idPair.Key);

                    if (export is not null)
                    {
                        // Retrieve image from external storage
                        var token = export.ProfilePicture.ProfilePictureRequest.Token;
                        var encodedImage = await _storageService.GetBase64EncodedFile(token);

                        // Build payload
                        var payload = JsonConvert.SerializeObject(new { content = encodedImage });
                        var httpContent = new StringContent(payload, Encoding.UTF8, "application/json");

                        // Send POST request
                        var response = await _httpClient.PostAsync($"{endpointUrl}{idPair.Value}/picture", httpContent);

                        if (response.IsSuccessStatusCode)
                        {
                            exportedProfilePictures.Add(export.ProfilePicture);
                            _dbContext.PendingExports.Remove(export);
                        }
                    }
                }
            }
            await _dbContext.SaveChangesAsync();
        }
        return exportedProfilePictures;
    }

    private async Task<Dictionary<string, string>[]> GetPersonIds(IEnumerable<Person> persons)
    {
        var publicIdentifiers = persons.Select(person => person.PublicIdentifier).ToArray();
        var filter = GetIdNrFilter(publicIdentifiers);
        var query = HttpUtility.ParseQueryString($"?filter={filter}");

        int endpointIndex = 0;
        int resolvedPersonIds = 0;
        Dictionary<string, string>[] personIds = Enumerable.Range(1, _personEndpoints.Length).Select(i => new Dictionary<string, string>()).ToArray();

        do
        {
            var endpoint = _personEndpoints[endpointIndex];
            var path = endpoint.Remove(endpoint.Length - 1) + "?" + query;
            var centerboardPersons = await _httpClient.GetFromJsonAsync<List<CenterboardPersonIdResponseDTO>>(path);
            if (centerboardPersons is not null && centerboardPersons.Count > 0)
            {
                foreach (var person in centerboardPersons)
                {
                    personIds[endpointIndex].Add(person.IdNr, person.Id);
                    resolvedPersonIds++;
                }
            }
            endpointIndex++;
        }
        while (endpointIndex < _personEndpoints.Length && resolvedPersonIds < persons.Count());

        return personIds;
    }

    private static string GetIdNrFilter(string[] publicIdentifiers)
    {
        var filterParts = publicIdentifiers.Select(identifier => $"idNr=={identifier}").ToArray();
        return string.Join(",", filterParts);
    }

    public void Dispose()
    {
        _httpClient.Dispose();
    }
}