using System.Text;
using System.Text.Json;
using Microsoft.Extensions.Options;
using ProfilePictureDataStore.Models.DataTransferObjects.Request;
using ProfilePictureDataStore.Models.Entities;
using ProfilePictureDataStore.Utilities;

namespace ProfilePictureDataStore.Services;

public class RemoteInternalEmailNotificationService(
    IOptions<EmailNotificationConfiguration> emailNotificationConfiguration,
    ICloudStorageService _cloudStorageService) : IEmailNotificationService
{
    private readonly EmailNotificationConfiguration _emailNotificationConfiguration = emailNotificationConfiguration.Value;

    public CreateEmailNotificationRequest GetEmailNotificationRequest(ProfilePictureRequest profilePictureRequest, bool sendImmediately = false)
    {
        var recipient = new System.Net.Mail.MailAddress(profilePictureRequest.Person.EmailAddress, profilePictureRequest.Person.FullName);
        var messageData = new Dictionary<string, object>
        {
            {"FULL_NAME", profilePictureRequest.Person.FullName},
            {"PROFILE_PICTURE_URL", GetRequestUrl(profilePictureRequest)},
        };

        bool sendToday = false;
        string templateIdentifier;
        var rejectedReason = profilePictureRequest.PreviousRequest?.ProfilePicture?.Review?.RejectReason;
        if (profilePictureRequest.PreviousRequest is not null && rejectedReason is not null)
        {
            // It's a follow-up request since there's a reject reason for a previously rejected profile picture.
            sendToday = true;
            templateIdentifier = _emailNotificationConfiguration.FollowUpRequestTemplateIdentifier;
            messageData.Add("REJECTED_REASON_EXPLANATION", rejectedReason.Explanation);
        }
        else
        {
            // It seems to be an initial request.
            // At least there's no reject reason for a previously rejected profile picture.
            templateIdentifier = _emailNotificationConfiguration.InitialRequestTemplateIdentifier;
        }

        // The null-value stands for immediate emission of the email.
        DateTime? scheduledSendingDateTimeUtc = null;

        if (!sendImmediately)
        {
            // Calculate local scheduled date/time and convert it to UTC
            var scheduledSendingDateTime = CalculateLocalScheduledSendDateAndTime(sendToday);
            var localTimezone = TimeZoneInfo.FindSystemTimeZoneById("Europe/Zurich");
            scheduledSendingDateTimeUtc = TimeZoneInfo.ConvertTimeToUtc(scheduledSendingDateTime, localTimezone);
        }

        return new CreateEmailNotificationRequest(
            templateIdentifier,
            recipient,
            messageData,
            scheduledSendingDateTimeUtc
        );
    }

    /// <summary>
    /// Creates an email notification request which notifies the creator of the provided profilePicture about the successful review.
    /// </summary>
    /// <param name="profilePicture">The profile picture object which was accepted in the manual review</param>
    /// <returns>The task resolving to the actual email notification request.</returns>
    public async Task<CreateEmailNotificationRequest> GetEmailNotificationRequestAsync(ProfilePicture profilePicture)
    {
        var validityDuration = (int)TimeSpan.FromDays(7).TotalSeconds;

        TimeZoneInfo localTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Europe/Zurich");
        var endDateUtc = DateTime.UtcNow.AddSeconds(validityDuration);
        var endDateLocal = TimeZoneInfo.ConvertTimeFromUtc(endDateUtc, localTimeZone);

        var signedDownloadUrl = await _cloudStorageService.GetSignedUrlAsync(profilePicture.FileName, validityDuration);

        var recipient = new System.Net.Mail.MailAddress(profilePicture.Person.EmailAddress, profilePicture.Person.FullName);
        var messageData = new Dictionary<string, object>
        {
            {"FULL_NAME", profilePicture.Person.FullName},
            {"PROFILE_PICTURE_DOWNLOAD_URL", signedDownloadUrl},
            {"DOWNLOAD_LINK_VALIDITY_END_DATE", endDateLocal.ToString("dd.MM.yyyy")},
            {"DOWNLOAD_LINK_VALIDITY_END_TIME", endDateLocal.ToString("HH:mm")},
        };

        return new CreateEmailNotificationRequest(_emailNotificationConfiguration.ReviewAcceptedTemplateIdentifier, recipient, messageData, null);
    }


    /// <summary>
    /// Sends a http POST request containing the email notifications data to the corresponding endpoint.
    /// </summary>
    /// <param name="notificationRequests"></param>
    /// <returns></returns>
    public async Task SendEmailNotificationRequestsAsync(IEnumerable<CreateEmailNotificationRequest> notificationRequests)
    {
        var jsonPayload = JsonSerializer.Serialize(notificationRequests);
        var requestBody = new StringContent(jsonPayload, Encoding.UTF8, "application/json");

        using var httpClient = new HttpClient();
        var response = await httpClient.PostAsync(_emailNotificationConfiguration.EmailServiceEndpoint, requestBody);

        if (!response.IsSuccessStatusCode)
        {
            var reason = await response.Content.ReadAsStringAsync();
            throw new ArgumentException(reason);
        }
    }

    /// <summary>
    /// Calculates the exact date and time for sending out an email based on various configuration variables.
    /// By default, the email is configured to be sent out the next day. This behavior can be disabled
    /// using the optional parameter.
    /// </summary>
    /// <param name="sendToday">An optional parameter to determine whether the default delay of 1 day should be ignored.</param>
    /// <returns>The local date and time for the email to be sent.</returns>
    private DateTime CalculateLocalScheduledSendDateAndTime(bool sendToday = false)
    {
        // By default, the email should be sent out the next day.
        var daysOffset = sendToday ? 0 : 1;
        var sendOutDay = DateTime.Today.AddDays(daysOffset);

        // Read configuration values.
        var sendingWeekdays = _emailNotificationConfiguration.SendingWeekdays.Split(',').Select(weekdayIndex => (DayOfWeek)int.Parse(weekdayIndex));
        var minSendTime = TimeOnly.Parse(_emailNotificationConfiguration.SendingTimeStart);
        var maxSendTime = TimeOnly.Parse(_emailNotificationConfiguration.SendingTimeEnd);

        var scheduledSendingDateTime = new DateTime(DateOnly.FromDateTime(sendOutDay), minSendTime);

        do
        {
            // Check whether today is a valid day to send out email notifications
            if (sendingWeekdays.Contains(scheduledSendingDateTime.DayOfWeek))
            {
                // Check whether now lies within the the defined window to send out email notifications.
                if (scheduledSendingDateTime.TimeOfDay >= minSendTime.ToTimeSpan() && scheduledSendingDateTime.TimeOfDay <= maxSendTime.ToTimeSpan())
                {
                    // Indicate to send the email notification immediately by returning null.
                    return scheduledSendingDateTime;
                }
                else if (scheduledSendingDateTime.TimeOfDay < minSendTime.ToTimeSpan())
                {
                    // It's too early to send out the email notification => delay until the minSendTime
                    return new DateTime(DateOnly.FromDateTime(scheduledSendingDateTime), minSendTime);
                }
            }

            // It's either to late or no valid day to send out the email notification now.
            // Delay the sending to the next day and continue checking whether that's a valid option.
            scheduledSendingDateTime = new DateTime(DateOnly.FromDateTime(scheduledSendingDateTime.AddDays(1)), minSendTime);
        } while (true);
    }

    private string GetRequestUrl(ProfilePictureRequest request)
    {
        var template = _emailNotificationConfiguration.ProfilePictureDeepLinkTemplate;
        return template.Replace("{TOKEN}", request.Token);
    }
}