using ProfilePictureDataStore.Models.Entities;

namespace ProfilePictureDataStore.Services;

public interface IProfilePictureRequestService
{
    public IEnumerable<ProfilePictureRequest> GenerateInitialRequests(IEnumerable<Person> persons);

    public IEnumerable<ProfilePictureRequest> GenerateFollowUpRequests(IEnumerable<ProfilePictureRequest> requests);
}