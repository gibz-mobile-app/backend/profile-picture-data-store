using ProfilePictureDataStore.Models.Entities;

namespace ProfilePictureDataStore.Services;

public class ProfilePictureRequestService : IProfilePictureRequestService
{
    public IEnumerable<ProfilePictureRequest> GenerateInitialRequests(IEnumerable<Person> persons)
    {
        var expiryDate = GetRequestExpiryDate();

        List<ProfilePictureRequest> profilePictureRequests = [];
        Ulid ulid;
        foreach (var person in persons)
        {
            ulid = Ulid.NewUlid();
            profilePictureRequests.Add(new ProfilePictureRequest
            {
                Token = ulid.ToString(),
                Person = person,
                ExpiryDate = expiryDate
            });
        }

        return profilePictureRequests;
    }

    public IEnumerable<ProfilePictureRequest> GenerateFollowUpRequests(IEnumerable<ProfilePictureRequest> requests)
    {
        var expiryDate = GetRequestExpiryDate();

        List<ProfilePictureRequest> profilePictureRequests = [];
        Ulid ulid;
        foreach (var request in requests)
        {
            ulid = Ulid.NewUlid();
            profilePictureRequests.Add(new ProfilePictureRequest
            {
                Token = ulid.ToString(),
                Person = request.Person,
                ExpiryDate = expiryDate,
                PreviousRequest = request,
                RequestSequenceIndex = request.RequestSequenceIndex + 1,
            });
        }

        return profilePictureRequests;
    }

    private static DateOnly GetRequestExpiryDate()
    {
        return DateOnly.FromDateTime(DateTime.Now).AddDays(60);
    }
}