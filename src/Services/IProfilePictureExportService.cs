using ProfilePictureDataStore.Models.Entities;

namespace ProfilePictureDataStore.Services;

public interface IProfilePictureExportService
{
    public Task<IEnumerable<ProfilePicture>> ProcessPendingExportsAsync();
}