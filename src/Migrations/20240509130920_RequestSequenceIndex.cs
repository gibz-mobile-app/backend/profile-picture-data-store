﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ProfilePictureDataStore.Migrations
{
    /// <inheritdoc />
    public partial class RequestSequenceIndex : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "RequestSequenceIndex",
                table: "ProfilePictureRequests",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RequestSequenceIndex",
                table: "ProfilePictureRequests");
        }
    }
}
