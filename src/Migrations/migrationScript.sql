﻿CREATE TABLE IF NOT EXISTS `__EFMigrationsHistory` (
    `MigrationId` varchar(150) CHARACTER SET utf8mb4 NOT NULL,
    `ProductVersion` varchar(32) CHARACTER SET utf8mb4 NOT NULL,
    CONSTRAINT `PK___EFMigrationsHistory` PRIMARY KEY (`MigrationId`)
) CHARACTER SET=utf8mb4;

START TRANSACTION;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20240423204346_InitialMigration') THEN

    ALTER DATABASE CHARACTER SET utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20240423204346_InitialMigration') THEN

    CREATE TABLE `Cohorts` (
        `Id` char(36) COLLATE ascii_general_ci NOT NULL,
        `UniqueName` longtext CHARACTER SET utf8mb4 NOT NULL,
        `DisplayName` longtext CHARACTER SET utf8mb4 NOT NULL,
        CONSTRAINT `PK_Cohorts` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20240423204346_InitialMigration') THEN

    CREATE TABLE `RejectReasons` (
        `Id` char(36) COLLATE ascii_general_ci NOT NULL,
        `Title` longtext CHARACTER SET utf8mb4 NOT NULL,
        `Explanation` longtext CHARACTER SET utf8mb4 NOT NULL,
        CONSTRAINT `PK_RejectReasons` PRIMARY KEY (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20240423204346_InitialMigration') THEN

    CREATE TABLE `Persons` (
        `Id` char(36) COLLATE ascii_general_ci NOT NULL,
        `PublicIdentifier` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
        `UserName` longtext CHARACTER SET utf8mb4 NOT NULL,
        `FirstName` longtext CHARACTER SET utf8mb4 NOT NULL,
        `LastName` longtext CHARACTER SET utf8mb4 NOT NULL,
        `CohortId` char(36) COLLATE ascii_general_ci NULL,
        CONSTRAINT `PK_Persons` PRIMARY KEY (`Id`),
        CONSTRAINT `FK_Persons_Cohorts_CohortId` FOREIGN KEY (`CohortId`) REFERENCES `Cohorts` (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20240423204346_InitialMigration') THEN

    CREATE TABLE `ProfilePictureRequests` (
        `Id` char(36) COLLATE ascii_general_ci NOT NULL,
        `Token` longtext CHARACTER SET utf8mb4 NOT NULL,
        `PersonId` char(36) COLLATE ascii_general_ci NOT NULL,
        `RequestState` int NOT NULL,
        `ValidationState` int NOT NULL,
        `ExpiryDate` date NOT NULL,
        `PreviousRequestId` char(36) COLLATE ascii_general_ci NULL,
        CONSTRAINT `PK_ProfilePictureRequests` PRIMARY KEY (`Id`),
        CONSTRAINT `FK_ProfilePictureRequests_Persons_PersonId` FOREIGN KEY (`PersonId`) REFERENCES `Persons` (`Id`) ON DELETE CASCADE,
        CONSTRAINT `FK_ProfilePictureRequests_ProfilePictureRequests_PreviousReques~` FOREIGN KEY (`PreviousRequestId`) REFERENCES `ProfilePictureRequests` (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20240423204346_InitialMigration') THEN

    CREATE TABLE `ProfilePictures` (
        `Id` char(36) COLLATE ascii_general_ci NOT NULL,
        `PersonId` char(36) COLLATE ascii_general_ci NOT NULL,
        `FileName` longtext CHARACTER SET utf8mb4 NOT NULL,
        `UploadDateTime` datetime(6) NOT NULL,
        `ProfilePictureRequestId` char(36) COLLATE ascii_general_ci NOT NULL,
        CONSTRAINT `PK_ProfilePictures` PRIMARY KEY (`Id`),
        CONSTRAINT `FK_ProfilePictures_Persons_PersonId` FOREIGN KEY (`PersonId`) REFERENCES `Persons` (`Id`) ON DELETE CASCADE,
        CONSTRAINT `FK_ProfilePictures_ProfilePictureRequests_ProfilePictureRequest~` FOREIGN KEY (`ProfilePictureRequestId`) REFERENCES `ProfilePictureRequests` (`Id`) ON DELETE CASCADE
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20240423204346_InitialMigration') THEN

    CREATE TABLE `ValidationFlag` (
        `Id` char(36) COLLATE ascii_general_ci NOT NULL,
        `Label` longtext CHARACTER SET utf8mb4 NOT NULL,
        `Value` float NULL,
        `ProfilePictureRequestId` char(36) COLLATE ascii_general_ci NULL,
        CONSTRAINT `PK_ValidationFlag` PRIMARY KEY (`Id`),
        CONSTRAINT `FK_ValidationFlag_ProfilePictureRequests_ProfilePictureRequestId` FOREIGN KEY (`ProfilePictureRequestId`) REFERENCES `ProfilePictureRequests` (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20240423204346_InitialMigration') THEN

    CREATE TABLE `ProfilePictureReviews` (
        `Id` char(36) COLLATE ascii_general_ci NOT NULL,
        `ReviewerId` char(36) COLLATE ascii_general_ci NOT NULL,
        `ReviewerUserName` longtext CHARACTER SET utf8mb4 NOT NULL,
        `ProfilePictureId` char(36) COLLATE ascii_general_ci NOT NULL,
        `ReviewState` int NOT NULL,
        `ReviewDateTime` datetime(6) NOT NULL,
        `RejectReasonId` char(36) COLLATE ascii_general_ci NULL,
        CONSTRAINT `PK_ProfilePictureReviews` PRIMARY KEY (`Id`),
        CONSTRAINT `FK_ProfilePictureReviews_ProfilePictures_ProfilePictureId` FOREIGN KEY (`ProfilePictureId`) REFERENCES `ProfilePictures` (`Id`) ON DELETE CASCADE,
        CONSTRAINT `FK_ProfilePictureReviews_RejectReasons_RejectReasonId` FOREIGN KEY (`RejectReasonId`) REFERENCES `RejectReasons` (`Id`)
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20240423204346_InitialMigration') THEN

    CREATE INDEX `IX_Persons_CohortId` ON `Persons` (`CohortId`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20240423204346_InitialMigration') THEN

    CREATE UNIQUE INDEX `IX_Persons_PublicIdentifier` ON `Persons` (`PublicIdentifier`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20240423204346_InitialMigration') THEN

    CREATE INDEX `IX_ProfilePictureRequests_PersonId` ON `ProfilePictureRequests` (`PersonId`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20240423204346_InitialMigration') THEN

    CREATE INDEX `IX_ProfilePictureRequests_PreviousRequestId` ON `ProfilePictureRequests` (`PreviousRequestId`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20240423204346_InitialMigration') THEN

    CREATE UNIQUE INDEX `IX_ProfilePictureReviews_ProfilePictureId` ON `ProfilePictureReviews` (`ProfilePictureId`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20240423204346_InitialMigration') THEN

    CREATE INDEX `IX_ProfilePictureReviews_RejectReasonId` ON `ProfilePictureReviews` (`RejectReasonId`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20240423204346_InitialMigration') THEN

    CREATE INDEX `IX_ProfilePictures_PersonId` ON `ProfilePictures` (`PersonId`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20240423204346_InitialMigration') THEN

    CREATE UNIQUE INDEX `IX_ProfilePictures_ProfilePictureRequestId` ON `ProfilePictures` (`ProfilePictureRequestId`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20240423204346_InitialMigration') THEN

    CREATE INDEX `IX_ValidationFlag_ProfilePictureRequestId` ON `ValidationFlag` (`ProfilePictureRequestId`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20240423204346_InitialMigration') THEN

    INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`)
    VALUES ('20240423204346_InitialMigration', '8.0.4');

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

COMMIT;

START TRANSACTION;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20240425194109_AddReviewClaim') THEN

    ALTER TABLE `ProfilePictureReviews` MODIFY COLUMN `ReviewerUserName` longtext CHARACTER SET utf8mb4 NULL;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20240425194109_AddReviewClaim') THEN

    ALTER TABLE `ProfilePictureReviews` MODIFY COLUMN `ReviewerId` char(36) COLLATE ascii_general_ci NULL;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20240425194109_AddReviewClaim') THEN

    CREATE TABLE `ReviewClaims` (
        `Id` char(36) COLLATE ascii_general_ci NOT NULL,
        `ProfilePictureId` char(36) COLLATE ascii_general_ci NOT NULL,
        `ReviewerId` char(36) COLLATE ascii_general_ci NOT NULL,
        `ClaimDateTime` datetime(6) NOT NULL,
        CONSTRAINT `PK_ReviewClaims` PRIMARY KEY (`Id`),
        CONSTRAINT `FK_ReviewClaims_ProfilePictures_ProfilePictureId` FOREIGN KEY (`ProfilePictureId`) REFERENCES `ProfilePictures` (`Id`) ON DELETE CASCADE
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20240425194109_AddReviewClaim') THEN

    CREATE UNIQUE INDEX `IX_ReviewClaims_ProfilePictureId` ON `ReviewClaims` (`ProfilePictureId`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20240425194109_AddReviewClaim') THEN

    INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`)
    VALUES ('20240425194109_AddReviewClaim', '8.0.4');

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

COMMIT;

START TRANSACTION;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20240426200956_CreatedAtForPersons') THEN

    ALTER TABLE `Persons` ADD `CreatedAt` datetime(6) NOT NULL DEFAULT '0001-01-01 00:00:00';

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20240426200956_CreatedAtForPersons') THEN

    INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`)
    VALUES ('20240426200956_CreatedAtForPersons', '8.0.4');

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

COMMIT;

START TRANSACTION;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20240509130920_RequestSequenceIndex') THEN

    ALTER TABLE `ProfilePictureRequests` ADD `RequestSequenceIndex` int NOT NULL DEFAULT 0;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20240509130920_RequestSequenceIndex') THEN

    INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`)
    VALUES ('20240509130920_RequestSequenceIndex', '8.0.4');

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

COMMIT;

START TRANSACTION;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20240529182144_Exports') THEN

    CREATE TABLE `PendingExports` (
        `Id` char(36) COLLATE ascii_general_ci NOT NULL,
        `ProfilePictureId` char(36) COLLATE ascii_general_ci NOT NULL,
        CONSTRAINT `PK_PendingExports` PRIMARY KEY (`Id`),
        CONSTRAINT `FK_PendingExports_ProfilePictures_ProfilePictureId` FOREIGN KEY (`ProfilePictureId`) REFERENCES `ProfilePictures` (`Id`) ON DELETE CASCADE
    ) CHARACTER SET=utf8mb4;

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20240529182144_Exports') THEN

    CREATE INDEX `IX_PendingExports_ProfilePictureId` ON `PendingExports` (`ProfilePictureId`);

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

DROP PROCEDURE IF EXISTS MigrationsScript;
DELIMITER //
CREATE PROCEDURE MigrationsScript()
BEGIN
    IF NOT EXISTS(SELECT 1 FROM `__EFMigrationsHistory` WHERE `MigrationId` = '20240529182144_Exports') THEN

    INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`)
    VALUES ('20240529182144_Exports', '8.0.4');

    END IF;
END //
DELIMITER ;
CALL MigrationsScript();
DROP PROCEDURE MigrationsScript;

COMMIT;

