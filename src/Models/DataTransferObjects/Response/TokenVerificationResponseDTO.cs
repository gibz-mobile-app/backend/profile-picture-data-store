namespace ProfilePictureDataStore.Models.DataTransferObjects.Response;

public record TokenVerificationResponseDTO(
    string Token,
    Guid? RequestId,
    EProfilePictureRequestState? RequestState,
    DateOnly? RequestExpiry

);
