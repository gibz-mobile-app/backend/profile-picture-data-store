namespace ProfilePictureDataStore.Models.DataTransferObjects.Response;

public record CohortResponseDTO(
    Guid Id,
    string UniqueName,
    string DisplayName
);