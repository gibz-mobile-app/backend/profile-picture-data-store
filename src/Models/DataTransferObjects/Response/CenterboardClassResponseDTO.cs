namespace ProfilePictureDataStore.Models.DataTransferObjects.Response;

public record CenterboardClassResponseDTO(
    string Id,
    string Token,
    string Semester
);