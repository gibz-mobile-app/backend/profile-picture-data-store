namespace ProfilePictureDataStore.Models.DataTransferObjects.Response;

public record ProfilePictureRequestResponseDTO(
    Guid Id,
    string Token,
    Guid PersonId,
    EProfilePictureRequestState RequestState,
    EValidationState ValidationState,
    DateOnly ExpiryDate,
    Guid? PreviousRequestId,
    int RequestSequenceIndex,
    PersonResponseDTO? Person = null,
    CohortResponseDTO? Cohort = null
);
