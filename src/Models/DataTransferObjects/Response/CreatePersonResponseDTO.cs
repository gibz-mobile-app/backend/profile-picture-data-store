namespace ProfilePictureDataStore.Models.DataTransferObjects.Response;

public record CreatePersonResponseDTO(
    int ProvidedPersonCount,
    int CreatedCount,
    int DuplicateCount,
    string[] CreatedGibs,
    string[] DuplicateGibs
);