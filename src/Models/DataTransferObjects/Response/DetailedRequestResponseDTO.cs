namespace ProfilePictureDataStore.Models.DataTransferObjects.Response;

public record DetailedRequestResponseDTO(
    ProfilePictureRequestResponseDTO Request,
    ProfilePictureResponseDTO? ProfilePicture = null,
    ProfilePictureReviewResponseDTO? ProfilePictureReview = null,
    RejectReasonResponseDTO? RejectReason = null
);
