namespace ProfilePictureDataStore.Models.DataTransferObjects.Response;

public record ProfilePictureResponseDTO(
    Guid Id,
    Guid PersonId,
    string FileName,
    DateTime UploadDateTime,
    Guid ProfilePictureRequestId,
    string DownloadURL
);
