using ProfilePictureDataStore.Models.Entities;

namespace ProfilePictureDataStore.Models.DataTransferObjects.Response;

public record ImageResponse(
    Guid Id,
    string Url,
    DateTime UploadedAt,
    string SubjectFirstName,
    string SubjectLastName,
    string? CohortUniqueName,
    string? CohortDisplayName,
    IEnumerable<ValidationFlag>? ValidationFlags
);