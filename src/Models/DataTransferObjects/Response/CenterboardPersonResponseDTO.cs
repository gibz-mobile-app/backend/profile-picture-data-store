namespace ProfilePictureDataStore.Models.DataTransferObjects.Response;

public record CenterboardPersonResponseDTO(
    string Id, // this is the centerboard-specific identifier
    string IdNr,    // this is the ID which is used at GIBZ
    string LastName,
    string FirstName,
    string UserName,
    CenterboardClassResponseDTO[]? RegularClasses
);