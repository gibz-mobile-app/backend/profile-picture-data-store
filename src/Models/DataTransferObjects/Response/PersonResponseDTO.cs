namespace ProfilePictureDataStore.Models.DataTransferObjects.Response;

public record PersonResponseDTO(
    Guid Id,
    string PublicIdentifier,
    string UserName,
    string FirstName,
    string LastName,
    Guid? CohortId
);
