namespace ProfilePictureDataStore.Models.DataTransferObjects.Response;

public record FailedTokenVerificationResponseDTO(
    string Token,
    Guid? RequestId,
    EProfilePictureRequestState? RequestState,
    DateOnly? RequestExpiry,
    int StatusCode,
    string StatusMessage
) : TokenVerificationResponseDTO(Token, RequestId, RequestState, RequestExpiry);