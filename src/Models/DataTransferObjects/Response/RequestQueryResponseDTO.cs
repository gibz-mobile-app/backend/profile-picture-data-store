namespace ProfilePictureDataStore.Models.DataTransferObjects.Response;

public record RequestQueryResponseDTO(
    int Total,
    ICollection<ProfilePictureRequestResponseDTO> Requests
);
