namespace ProfilePictureDataStore.Models.DataTransferObjects.Response;

public record CenterboardPersonIdResponseDTO(
    string Id, // this is the centerboard-specific identifier
    string IdNr    // this is the ID which is used at GIBZ
);
