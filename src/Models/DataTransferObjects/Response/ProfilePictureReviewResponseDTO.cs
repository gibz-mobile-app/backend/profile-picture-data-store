namespace ProfilePictureDataStore.Models.DataTransferObjects.Response;

public record ProfilePictureReviewResponseDTO(
    Guid Id,
    Guid? ReviewerId,
    string? ReviewerUserName,
    Guid ProfilePictureId,
    EReviewState ReviewState,
    DateTime ReviewDateTime,
    Guid? RejectReasonId
);
