using ProfilePictureDataStore.Models.Entities;

namespace ProfilePictureDataStore.Models.DataTransferObjects.Response;

public record RejectReasonResponseDTO(Guid Id, string Title, string Explanation)
{
    public static RejectReasonResponseDTO FromRejectReason(RejectReason rejectReason)
    {
        return new RejectReasonResponseDTO(rejectReason.Id, rejectReason.Title, rejectReason.Explanation);
    }
}