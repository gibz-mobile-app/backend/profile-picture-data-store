namespace ProfilePictureDataStore.Models.DataTransferObjects.Response;
public record SuccessfulTokenVerificationResponseDTO(
    string Token,
    Guid? RequestId,
    EProfilePictureRequestState? RequestState,
    DateOnly? RequestExpiry,
    string FirstName,
    string LastName,
    string CohortUniqueName,
    string CohortDisplayName
) : TokenVerificationResponseDTO(Token, RequestId, RequestState, RequestExpiry);