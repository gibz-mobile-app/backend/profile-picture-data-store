using ProfilePictureDataStore.Models.Entities;

namespace ProfilePictureDataStore.Models.DataTransferObjects.Request;

public record UploadProfilePictureRequest(string Token, IFormFile ProfilePicture, ICollection<ValidationFlag>? ValidationFlags = null, bool doNotUpdateDatabase = false);