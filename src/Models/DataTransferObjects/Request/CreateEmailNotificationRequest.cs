using System.Net.Mail;

namespace ProfilePictureDataStore.Models.DataTransferObjects.Request;

public record CreateEmailNotificationRequest(
    string TemplateIdentifier,
    MailAddress Recipient,
    Dictionary<string, object> MessageData,
    DateTime? ScheduledSendDateTime
);
