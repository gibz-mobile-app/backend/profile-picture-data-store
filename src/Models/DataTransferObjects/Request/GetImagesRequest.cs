namespace ProfilePictureDataStore.Models.DataTransferObjects.Request;

public record GetImagesRequest(int Take, EReviewState? ReviewState);
