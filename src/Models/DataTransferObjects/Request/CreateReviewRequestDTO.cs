namespace ProfilePictureDataStore.Models.DataTransferObjects.Request;

public record CreateReviewRequestDTO(
    Guid ProfilePictureId,
    EReviewState ReviewState,
    Guid? RejectReasonId
);