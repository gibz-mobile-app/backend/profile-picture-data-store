namespace ProfilePictureDataStore.Models.DataTransferObjects.Request;

public record CreatePersonRequest(
    string Gib,
    string UserName,
    string FirstName,
    string LastName,
    string ClassUniqueName,
    string ClassDisplayName
);
