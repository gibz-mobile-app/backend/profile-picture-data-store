namespace ProfilePictureDataStore.Models;

public enum EProfilePictureRequestState
{
    Pending,
    Fulfilled,
    ValidatedInvalid,
    Expired
}