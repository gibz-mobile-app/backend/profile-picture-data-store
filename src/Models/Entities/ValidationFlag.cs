namespace ProfilePictureDataStore.Models.Entities;

public class ValidationFlag
{
    public Guid Id { get; set; }
    public string Label { get; set; } = null!;
    public float? Value { get; set; }
}