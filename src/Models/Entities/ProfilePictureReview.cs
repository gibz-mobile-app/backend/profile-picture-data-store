namespace ProfilePictureDataStore.Models.Entities;

public class ProfilePictureReview
{
    public Guid Id { get; set; }
    // public Guid ProfilePictureId { get; set; }
    // public virtual ProfilePicture ProfilePicture { get; set; } = null!;
    public Guid? ReviewerId { get; set; } = null;
    public string? ReviewerUserName { get; set; } = null!;
    public Guid ProfilePictureId { get; set; }
    public virtual ProfilePicture ProfilePicture { get; set; } = null!;
    public EReviewState ReviewState { get; set; } = EReviewState.Pending;
    public DateTime ReviewDateTime { get; set; }
    public Guid? RejectReasonId { get; set; }
    public virtual RejectReason? RejectReason { get; set; }
}