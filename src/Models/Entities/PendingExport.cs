namespace ProfilePictureDataStore.Models.Entities;

public class PendingExport
{
    public Guid Id { get; set; }
    public Guid ProfilePictureId { get; set; }
    public virtual ProfilePicture ProfilePicture { get; set; } = null!;
}