namespace ProfilePictureDataStore.Models.Entities;

public class Cohort
{
    public Guid Id { get; set; }
    public string UniqueName { get; set; } = null!;
    public string DisplayName { get; set; } = null!;
}