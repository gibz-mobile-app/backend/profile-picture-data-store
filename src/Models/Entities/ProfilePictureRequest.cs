namespace ProfilePictureDataStore.Models.Entities;

public class ProfilePictureRequest
{
    public Guid Id { get; set; }
    public string Token { get; set; } = null!;
    public Guid PersonId { get; set; }
    public virtual Person Person { get; set; } = null!;
    public EProfilePictureRequestState RequestState { get; set; } = EProfilePictureRequestState.Pending;
    public EValidationState ValidationState { get; set; } = EValidationState.Pending;
    public virtual ICollection<ValidationFlag> ValidationFlags { get; set; } = null!;
    public DateOnly ExpiryDate { get; set; }
    public virtual ProfilePicture? ProfilePicture { get; set; }
    public Guid? PreviousRequestId { get; set; }
    public virtual ProfilePictureRequest? PreviousRequest { get; set; }
    public int RequestSequenceIndex { get; set; } = 0;
}