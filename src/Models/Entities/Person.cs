namespace ProfilePictureDataStore.Models.Entities;

public class Person
{
    public Guid Id { get; set; }
    public string PublicIdentifier { get; set; } = null!;
    public string UserName { get; set; } = null!;
    public string FirstName { get; set; } = null!;
    public string LastName { get; set; } = null!;
    public Guid? CohortId { get; set; }
    public virtual Cohort? Cohort { get; set; } = null!;
    public DateTime CreatedAt { get; set; }
    public string EmailAddress => $"{UserName}@online.gibz.ch";
    public string FullName => $"{FirstName} {LastName}";
}
