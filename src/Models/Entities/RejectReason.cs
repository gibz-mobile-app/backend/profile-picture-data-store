namespace ProfilePictureDataStore.Models.Entities;

public class RejectReason
{
    public Guid Id { get; set; }
    public string Title { get; set; } = null!;
    public string Explanation { get; set; } = null!;
}