namespace ProfilePictureDataStore.Models.Entities;

public class ReviewClaim
{
    public Guid Id { get; set; }
    public Guid ProfilePictureId { get; set; }
    public virtual ProfilePicture ProfilePicture { get; set; } = null!;
    public Guid ReviewerId { get; set; }
    public DateTime ClaimDateTime { get; set; }
}