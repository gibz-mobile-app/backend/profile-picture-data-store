namespace ProfilePictureDataStore.Models.Entities;

public class ProfilePicture
{
    public Guid Id { get; set; }
    public Guid PersonId { get; set; }
    public virtual Person Person { get; set; } = null!;
    public string FileName { get; set; } = null!;
    public DateTime UploadDateTime { get; set; } = DateTime.Now;
    public virtual ProfilePictureReview Review { get; set; } = null!;
    public Guid ProfilePictureRequestId { get; set; }
    public virtual ProfilePictureRequest ProfilePictureRequest { get; set; } = null!;
    public virtual ReviewClaim ReviewClaim { get; set; } = null!;
}