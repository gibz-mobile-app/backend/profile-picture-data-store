namespace ProfilePictureDataStore.Models;

public enum EValidationState
{
    Pending,
    Passed,
    Failed
}
