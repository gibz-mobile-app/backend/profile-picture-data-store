namespace ProfilePictureDataStore.Models;

public enum EReviewState
{
    Pending,
    InProgress,
    Accepted,
    Rejected
}
