
using Microsoft.EntityFrameworkCore;
using ProfilePictureDataStore.Models.Entities;

namespace ProfilePictureDataStore.Models;

public class ProfilePictureDbContext(DbContextOptions<ProfilePictureDbContext> options) : DbContext(options)
{
    public DbSet<Cohort> Cohorts { get; set; }
    public DbSet<Person> Persons { get; set; }
    public DbSet<ProfilePictureRequest> ProfilePictureRequests { get; set; }
    public DbSet<ProfilePicture> ProfilePictures { get; set; }
    public DbSet<ProfilePictureReview> ProfilePictureReviews { get; set; }
    public DbSet<RejectReason> RejectReasons { get; set; }
    public DbSet<ReviewClaim> ReviewClaims { get; set; }
    public DbSet<PendingExport> PendingExports { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseLazyLoadingProxies();
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Person>()
            .HasIndex(person => person.PublicIdentifier)
            .IsUnique(true);
    }
}