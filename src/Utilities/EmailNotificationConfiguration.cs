namespace ProfilePictureDataStore.Utilities;

public class EmailNotificationConfiguration
{
    public string EmailServiceEndpoint { get; set; } = null!;
    public string InitialRequestTemplateIdentifier { get; set; } = null!;
    public string FollowUpRequestTemplateIdentifier { get; set; } = null!;
    public string NewReviewTemplateIdentifier { get; set; } = null!;
    public string ReviewAcceptedTemplateIdentifier { get; set; } = null!;

    public string SendingWeekdays { get; set; } = null!;
    public string SendingTimeStart { get; set; } = null!;
    public string SendingTimeEnd { get; set; } = null!;

    public string ProfilePictureDeepLinkTemplate { get; set; } = null!;

    public IEnumerable<ReviewNotificationRecipient> ReviewNotificationRecipients { get; set; } = [];
}