namespace ProfilePictureDataStore.Utilities;

public class GoogleCloudStorageConfigOptions
{
    public string? AuthFileLocation { get; set; }
    public string? BucketName { get; set; }
    public int SignedUrlValidityInSeconds { get; set; }
}