namespace ProfilePictureDataStore.Utilities;

public class CenterboardExportConfiguration
{
    public string BasicAuthUsername { get; set; } = null!;
    public string BasicAuthPassword { get; set; } = null!;
    public string BaseURL { get; set; } = null!;
    public string StudentsEndpoint { get; set; } = null!;
    public string TeachersEndpoint { get; set; } = null!;
    public string EmployeesEndpoint { get; set; } = null!;
    public string PictureEndpointSuffix { get; set; } = null!;
    public int BatchSize { get; set; }
}