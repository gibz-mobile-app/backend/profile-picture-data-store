namespace ProfilePictureDataStore.Utilities;

public class ReviewNotificationRecipient
{
    public string Name { get; set; } = null!;
    public string Email { get; set; } = null!;
}