using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProfilePictureDataStore.Models;

namespace ProfilePictureDataStore.Controllers.v1;

[ApiController]
[Route("v1/[controller]")]
public class ReviewClaimController(IConfiguration _configuration, ProfilePictureDbContext _dbContext) : ControllerBase
{
    /// <summary>
    /// This action removes all review claims older than some threshold from the database.
    /// This is required for reviews which were assigned to a specific user (because he/her loaded it initially)
    /// to become available for all users.
    /// </summary>
    /// <returns>Returns a http status of either 200 or 400, depending on whether the operation was successful.</returns>
    [HttpDelete]
    public async Task<IActionResult> DeleteReviewClaims()
    {
        var lifespanInMinutes = _configuration.GetValue<int>("ReviewClaimLifespanInMinutes");
        if (lifespanInMinutes > 0)
        {
            var deleteThreshold = DateTime.Now.Subtract(new TimeSpan(0, lifespanInMinutes, 0));
            var utcThreshold = deleteThreshold.ToUniversalTime();
            await _dbContext.ReviewClaims
                .Where(reviewClaim => reviewClaim.ClaimDateTime <= utcThreshold)
                .ExecuteDeleteAsync();

            return NoContent();
        }

        return BadRequest();
    }
}
