using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProfilePictureDataStore.Authorization;
using ProfilePictureDataStore.Models;
using ProfilePictureDataStore.Models.DataTransferObjects.Response;

namespace ProfilePictureDataStore.Controllers.v1;

[ApiController]
[Route("v1/[controller]")]
public class CohortController(ProfilePictureDbContext _dbContext) : ControllerBase
{
    [HttpGet]
    [Authorize(Policy = ReviewerRequirement.PolicyName)]
    public async Task<ActionResult<ICollection<CohortResponseDTO>>> GetCohorts()
    {
        return await _dbContext.Cohorts
            .Select(cohort => new CohortResponseDTO(
                cohort.Id,
                cohort.UniqueName,
                cohort.DisplayName)
            )
            .ToListAsync();
    }
}