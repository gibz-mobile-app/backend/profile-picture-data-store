using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProfilePictureDataStore.Models;
using ProfilePictureDataStore.Models.DataTransferObjects.Response;

namespace ProfilePictureDataStore.Controllers.v1;

[ApiController]
[Route("v1/[controller]")]
public class RejectReasonController(ProfilePictureDbContext dbContext) : ControllerBase
{
    private readonly ProfilePictureDbContext _dbContext = dbContext;

    [HttpGet]
    public async Task<IEnumerable<RejectReasonResponseDTO>> GetRejectReasons()
    {
        var rejectReasons = await _dbContext.RejectReasons.OrderBy(reason => reason.Title).ToListAsync();
        return rejectReasons.Select(RejectReasonResponseDTO.FromRejectReason);
    }
}