using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProfilePictureDataStore.Authorization;
using ProfilePictureDataStore.Models;
using ProfilePictureDataStore.Models.DataTransferObjects.Request;
using ProfilePictureDataStore.Models.DataTransferObjects.Response;
using ProfilePictureDataStore.Models.Entities;
using ProfilePictureDataStore.Services;

namespace ProfilePictureDataStore.Controllers.v1;

[ApiController]
[Route("v1/[controller]")]
public class PersonController(
    ProfilePictureDbContext dbContext,
    IProfilePictureRequestService _profilePictureRequestService,
    IEmailNotificationService _emailNotificationService
) : ControllerBase
{
    private readonly ProfilePictureDbContext _dbContext = dbContext;

    [HttpPost]
    [Authorize(Policy = "PersonCreationScope")]
    public async Task<ActionResult<CreatePersonResponseDTO>> CreatePerson(CreatePersonRequest[] createPersonRequest)
    {
        // Remove duplicates
        var newPersons = await ExtractNewPersonsAsync(createPersonRequest);
        var duplicateCount = createPersonRequest.Length - newPersons.Length;

        // Create cohorts based on provided person data.
        var cohorts = await CreateCohorts(newPersons);

        // Extract person data from request
        var persons = newPersons.Select(request => new Person
        {
            PublicIdentifier = request.Gib,
            UserName = request.UserName,
            FirstName = request.FirstName,
            LastName = request.LastName,
            Cohort = cohorts.FirstOrDefault(cohort => cohort.UniqueName.Equals(request.ClassUniqueName)),
            CreatedAt = DateTime.UtcNow
        }
        ).ToList();

        // Add person entities to the database.
        await _dbContext.Persons.AddRangeAsync(persons);

        // Generate a request to submit a profile picture for every person.
        var profilePictureRequests = _profilePictureRequestService.GenerateInitialRequests(persons);
        await _dbContext.ProfilePictureRequests.AddRangeAsync(profilePictureRequests);

        // Generate email notifications
        var notificationRequests = profilePictureRequests.Select(
            request => _emailNotificationService.GetEmailNotificationRequest(request)
        );
        await _emailNotificationService.SendEmailNotificationRequestsAsync(notificationRequests);

        // Persist changes
        await _dbContext.SaveChangesAsync();

        // Generate response
        var createdIds = newPersons.Select(person => person.Gib).ToArray();
        return Ok(new CreatePersonResponseDTO(
            createPersonRequest.Length,
            newPersons.Length,
            duplicateCount,
            createdIds,
            createPersonRequest.Select(person => person.Gib).Except(createdIds).ToArray()
        ));
    }

    [HttpGet]
    [Authorize(Policy = ReviewerRequirement.PolicyName)]
    public async Task<ActionResult<ICollection<PersonResponseDTO>>> GetPersons()
    {
        return await _dbContext.Persons
            .Select(person => new PersonResponseDTO(
                person.Id,
                person.PublicIdentifier,
                person.UserName,
                person.FirstName,
                person.LastName,
                person.CohortId)
            )
            .ToListAsync();
    }

    private async Task<CreatePersonRequest[]> ExtractNewPersonsAsync(CreatePersonRequest[] request)
    {
        var ids = request.Select(person => person.Gib);
        var existingIds = await _dbContext.Persons.Select(person => person.PublicIdentifier).Where(id => ids.Contains(id)).ToListAsync();
        return request.Where(person => !existingIds.Contains(person.Gib)).ToArray();
    }

    private async Task<IEnumerable<Cohort>> CreateCohorts(CreatePersonRequest[] newPersons)
    {
        // Extract cohort data from request
        var requestCohorts = newPersons.Select(person => new { DisplayName = person.ClassDisplayName, UniqueName = person.ClassUniqueName });
        var uniqueCohortNames = requestCohorts.Select(cohort => cohort.UniqueName);

        // Retrieve existing cohorts from database
        var existingCohorts = await _dbContext.Cohorts.Where(cohort => uniqueCohortNames.Contains(cohort.UniqueName)).ToListAsync();
        var missingCohorts = requestCohorts.ExceptBy(existingCohorts.Select(e => e.UniqueName), e => e.UniqueName).ToList();

        // Create missing cohorts
        var cohortsToCreate = missingCohorts.Select(requestData => new Cohort { UniqueName = requestData.UniqueName, DisplayName = requestData.DisplayName }).ToList();
        await _dbContext.Cohorts.AddRangeAsync(cohortsToCreate);
        existingCohorts.AddRange(cohortsToCreate);

        return existingCohorts;
    }
}