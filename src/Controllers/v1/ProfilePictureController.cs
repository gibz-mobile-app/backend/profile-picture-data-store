using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using ProfilePictureDataStore.Models;
using ProfilePictureDataStore.Models.DataTransferObjects.Request;
using ProfilePictureDataStore.Models.Entities;
using ProfilePictureDataStore.Services;
using ProfilePictureDataStore.Utilities;

namespace ProfilePictureDataStore.Controllers.v1;

[ApiController]
[Route("v1/[controller]")]
public class ProfilePictureController(
    ProfilePictureDbContext _dbContext,
    ICloudStorageService _cloudStorageService,
    IOptions<EmailNotificationConfiguration> emailNotificationConfiguration,
    IEmailNotificationService _emailNotificationService
) : ControllerBase
{
    private readonly EmailNotificationConfiguration _emailNotificationConfiguration = emailNotificationConfiguration.Value;

    [HttpPost]
    public async Task<IActionResult> UploadProfilePictureAsync([FromForm] UploadProfilePictureRequest uploadRequest)
    {
        // Check MIME type of provided image
        if (uploadRequest.ProfilePicture.ContentType != System.Net.Mime.MediaTypeNames.Image.Png)
        {
            return new UnsupportedMediaTypeResult() { };
        }

        // Check whether a corresponding ProfilePictureRequest is pending
        var pendingProfilePictureRequest = await _dbContext.ProfilePictureRequests
            .Where(request => request.RequestState == EProfilePictureRequestState.Pending && request.Token.Equals(uploadRequest.Token))
            .FirstOrDefaultAsync();
        if (pendingProfilePictureRequest is null)
        {
            return NotFound($"Invalid value for '{nameof(uploadRequest.Token)}'. No pending request found.");
        }

        // Upload image to Google Cloud Storage
        await _cloudStorageService.UploadFileAsync(uploadRequest.ProfilePicture, uploadRequest.Token);

        // If the image to be persisted was a test/preview image:
        // There's no need to update the database entities.  
        if (uploadRequest.doNotUpdateDatabase)
        {
            return Ok();
        }

        // Update profile picture request entry in database
        pendingProfilePictureRequest.RequestState = EProfilePictureRequestState.Fulfilled;
        pendingProfilePictureRequest.ValidationState = EValidationState.Passed;
        pendingProfilePictureRequest.ValidationFlags = uploadRequest.ValidationFlags ?? [];
        _dbContext.Update(pendingProfilePictureRequest);

        // Crate and persist profile picture record.
        var profilePicture = new ProfilePicture
        {
            PersonId = pendingProfilePictureRequest.PersonId,
            FileName = pendingProfilePictureRequest.Token,
            UploadDateTime = DateTime.Now,
            ProfilePictureRequest = pendingProfilePictureRequest,
        };
        await _dbContext.ProfilePictures.AddAsync(profilePicture);

        // Persist changes in database.
        await _dbContext.SaveChangesAsync();

        // If this is the only profile picture without review: Notify the reviewers
        var picturesWithoutReview = await _dbContext.ProfilePictures.Where(picture => picture.Review == null).CountAsync();
        if (picturesWithoutReview == 1)
        {
            await NotifyReviewers();
        }

        return Ok();
    }

    private async Task NotifyReviewers()
    {
        List<CreateEmailNotificationRequest> notificationRequests = [];
        foreach (var recipient in _emailNotificationConfiguration.ReviewNotificationRecipients)
        {
            notificationRequests.Add(
                new CreateEmailNotificationRequest(
                    _emailNotificationConfiguration.NewReviewTemplateIdentifier,
                    new System.Net.Mail.MailAddress(recipient.Email, recipient.Name),
                    new Dictionary<string, object> { { "FULL_NAME", recipient.Name } },
                    null
                )
            );
        }

        await _emailNotificationService.SendEmailNotificationRequestsAsync(notificationRequests);
    }
}