using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProfilePictureDataStore.Authorization;
using ProfilePictureDataStore.Models;
using ProfilePictureDataStore.Models.DataTransferObjects.Response;
using ProfilePictureDataStore.Models.Entities;
using ProfilePictureDataStore.Services;

namespace ProfilePictureDataStore.Controllers.v1;

[ApiController]
[Route("v1/[controller]")]
public class RequestController(ProfilePictureDbContext dbContext, ICloudStorageService _storageService) : ControllerBase
{
    private readonly ProfilePictureDbContext _dbContext = dbContext;

    [HttpGet]
    [Authorize(Policy = ReviewerRequirement.PolicyName)]
    public async Task<ActionResult<RequestQueryResponseDTO>> GetRequests(
        [FromQuery] int count = 20,
        [FromQuery] int page = 1,
        [FromQuery] string? search = null
    )
    {
        var query = _dbContext.ProfilePictureRequests
            .Include(request => request.Person)
            .ThenInclude(person => person.Cohort);

        IQueryable<ProfilePictureRequest> where;
        if (search is not null)
        {
            where = query.Where(request => request.Person.PublicIdentifier.Contains(search)
                                           || request.Person.UserName.Contains(search)
                                           || request.Person.FirstName.Contains(search)
                                           || request.Person.LastName.Contains(search));
        }
        else
        {
            where = query.Where(request => request.RequestState == EProfilePictureRequestState.Pending);
        }

        var result = await where
            .Skip((page - 1) * count)
            .Take(count)
            .ToListAsync();

        var resultCount = await where.CountAsync();

        return new RequestQueryResponseDTO(
            resultCount,
            result.Select(GetProfilePictureRequestResponseDTO).ToList()
        );
    }

    [HttpGet("{requestId}")]
    [Authorize(Policy = ReviewerRequirement.PolicyName)]
    public async Task<ActionResult<DetailedRequestResponseDTO?>> GetRequest(Guid requestId)
    {
        var request = await _dbContext.ProfilePictureRequests.Include(request => request.Person)
                                                             .ThenInclude(person => person.Cohort)
                                                             .Include(request => request.ProfilePicture)
                                                             .Where(request => request.Id == requestId)
                                                             .FirstOrDefaultAsync();

        if (request is null)
        {
            return NotFound();
        }

        var response = new DetailedRequestResponseDTO(GetProfilePictureRequestResponseDTO(request));

        if (request.ProfilePicture is not null)
        {
            response = response with
            {
                ProfilePicture = new ProfilePictureResponseDTO(
                    request.ProfilePicture.Id,
                    request.ProfilePicture.PersonId,
                    request.ProfilePicture.FileName,
                    request.ProfilePicture.UploadDateTime,
                    request.ProfilePicture.ProfilePictureRequestId,
                    await _storageService.GetSignedUrlAsync(request.ProfilePicture.FileName, 120)
                )
            };

            var review = await _dbContext.ProfilePictureReviews.Include(review => review.RejectReason)
                                                               .Where(review => review.ProfilePictureId == request.ProfilePicture.Id)
                                                               .FirstOrDefaultAsync();

            if (review is not null)
            {
                response = response with
                {
                    ProfilePictureReview = new ProfilePictureReviewResponseDTO(
                        review.Id,
                        review.ReviewerId,
                        review.ReviewerUserName,
                        review.ProfilePictureId,
                        review.ReviewState,
                        review.ReviewDateTime,
                        review.RejectReasonId
                    )
                };

                if (review.RejectReason is not null)
                {
                    response = response with
                    {
                        RejectReason = new RejectReasonResponseDTO(
                            review.RejectReason.Id,
                            review.RejectReason.Title,
                            review.RejectReason.Explanation
                        )
                    };
                }
            }
        }

        return response;
    }

    private ProfilePictureRequestResponseDTO GetProfilePictureRequestResponseDTO(ProfilePictureRequest request)
    {
        return new ProfilePictureRequestResponseDTO(
            request.Id,
            request.Token,
            request.PersonId,
            request.RequestState,
            request.ValidationState,
            request.ExpiryDate,
            request.PreviousRequestId,
            request.RequestSequenceIndex,
            new PersonResponseDTO(
                request.Person.Id,
                request.Person.PublicIdentifier,
                request.Person.UserName,
                request.Person.FirstName,
                request.Person.LastName,
                request.Person.CohortId
            ),
            new CohortResponseDTO(
                request.Person.Cohort!.Id,
                request.Person.Cohort.UniqueName,
                request.Person.Cohort.DisplayName
            )
        );
    }
}