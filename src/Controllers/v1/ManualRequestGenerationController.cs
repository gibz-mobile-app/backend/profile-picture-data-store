using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProfilePictureDataStore.Authorization;
using ProfilePictureDataStore.Models;
using ProfilePictureDataStore.Models.DataTransferObjects.Request;
using ProfilePictureDataStore.Models.Entities;
using ProfilePictureDataStore.Services;

namespace ProfilePictureDataStore.Controllers.v1;

[ApiController]
[Route("v1/[controller]")]
public class ManualRequestGenerationController(
    ProfilePictureDbContext _dbContext,
    IProfilePictureRequestService _profilePictureRequestService,
    IEmailNotificationService _emailNotificationService,
    IPersonQueryService _personQueryService
) : ControllerBase
{
    [HttpPost]
    // [Authorize(Policy = ReviewerRequirement.PolicyName)]
    public async Task<ActionResult<CreatePersonRequest>> GenerateProfilePictureRequest(CreatePersonRequest createPersonRequest)
    {
        // Check if person already exists in database
        var person = await _dbContext.Persons.Include(person => person.Cohort)
                                                     .Where(person => person.PublicIdentifier == createPersonRequest.Gib)
                                                     .FirstOrDefaultAsync();

        // Add person to database if he/she does not exist yet
        if (person is null)
        {
            // Check if cohort already exists
            var cohort = await _dbContext.Cohorts.Where(cohort => cohort.UniqueName == createPersonRequest.ClassUniqueName)
                                                         .FirstOrDefaultAsync() ?? new Cohort
                                                         {
                                                             UniqueName = createPersonRequest.ClassUniqueName,
                                                             DisplayName = createPersonRequest.ClassDisplayName
                                                         };

            // Create new person object with data provided in the request body
            person = new Person
            {
                PublicIdentifier = createPersonRequest.Gib,
                UserName = createPersonRequest.UserName,
                FirstName = createPersonRequest.FirstName,
                LastName = createPersonRequest.LastName,
                Cohort = cohort
            };

            // Add person to database
            await _dbContext.Persons.AddAsync(person);
        }

        // Generate a request to submit a profile picture
        var profilePictureRequests = _profilePictureRequestService.GenerateInitialRequests([person]);
        await _dbContext.ProfilePictureRequests.AddRangeAsync(profilePictureRequests);

        // Generate email notifications
        var notificationRequests = profilePictureRequests.Select(
            request => _emailNotificationService.GetEmailNotificationRequest(request, sendImmediately: true)
        );
        await _emailNotificationService.SendEmailNotificationRequestsAsync(notificationRequests);

        // Persist changes
        await _dbContext.SaveChangesAsync();

        return createPersonRequest;
    }

    [HttpGet]
    // [Authorize(Policy = ReviewerRequirement.PolicyName)]
    public async Task<ActionResult<List<CreatePersonRequest>>> GetPersonData([FromQuery] string searchProperty, [FromQuery] string searchValue)
    {
        var persons = await _personQueryService.FindPersonsAsync(searchProperty, searchValue);
        if (persons is null || persons.Count() == 0)
        {
            return NotFound();
        }

        return persons;
    }
}
