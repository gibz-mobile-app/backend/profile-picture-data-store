using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProfilePictureDataStore.Models;
using ProfilePictureDataStore.Models.DataTransferObjects.Response;

namespace ProfilePictureDataStore.Controllers.v1;

[ApiController]
[Route("v1/[controller]")]
public class TokenController(ProfilePictureDbContext dbContext) : ControllerBase
{
    private readonly ProfilePictureDbContext _dbContext = dbContext;

    [HttpGet]
    public async Task<ActionResult<TokenVerificationResponseDTO>> VerifyToken([FromQuery] string verify)
    {
        var token = verify;
        var request = await _dbContext.ProfilePictureRequests
            .Include(request => request.Person)
            .ThenInclude(person => person.Cohort)
            .FirstOrDefaultAsync(request => request.Token == token);

        if (request is null)
        {
            return NotFound();
        }

        var statusCode = HttpStatusCode.OK;
        TokenVerificationResponseDTO response;

        if (request.RequestState != EProfilePictureRequestState.Pending)
        {
            statusCode = HttpStatusCode.Conflict;
            response = new FailedTokenVerificationResponseDTO(
                token,
                request.Id,
                request.RequestState,
                request.ExpiryDate,
                11,
                "The request has already been processed."
            );
        }
        else if (request.ExpiryDate < DateOnly.FromDateTime(DateTime.Now))
        {
            statusCode = HttpStatusCode.Gone;
            response = new FailedTokenVerificationResponseDTO(
                token,
                request.Id,
                request.RequestState,
                request.ExpiryDate,
                12,
                "The request has expired."
            );
        }
        else
        {
            response = new SuccessfulTokenVerificationResponseDTO(
                request.Token,
                request.Id,
                request.RequestState,
                request.ExpiryDate,
                request.Person.FirstName,
                request.Person.LastName,
                request.Person.Cohort?.UniqueName ?? "",
                request.Person.Cohort?.DisplayName ?? ""
            );
        }

        return StatusCode((int)statusCode, response);
    }
}
