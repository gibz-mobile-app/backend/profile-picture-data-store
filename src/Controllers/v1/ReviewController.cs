using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using ProfilePictureDataStore.Authorization;
using ProfilePictureDataStore.Models;
using ProfilePictureDataStore.Models.DataTransferObjects.Request;
using ProfilePictureDataStore.Models.DataTransferObjects.Response;
using ProfilePictureDataStore.Models.Entities;
using ProfilePictureDataStore.Services;
using ProfilePictureDataStore.Utilities;

namespace ProfilePictureDataStore.Controllers.v1;

[ApiController]
[Route("v1/[controller]")]
public class ReviewController(
    ProfilePictureDbContext dbContext,
    IReviewService reviewService,
    ICloudStorageService cloudStorageService,
    IEmailNotificationService emailNotificationService,
    IOptions<GoogleCloudStorageConfigOptions> googleCloudStorageConfigOptions) : ControllerBase
{
    private readonly ProfilePictureDbContext _dbContext = dbContext;
    private readonly IReviewService _reviewService = reviewService;
    private readonly ICloudStorageService _cloudStorageService = cloudStorageService;
    private readonly IEmailNotificationService _emailNotificationService = emailNotificationService;
    private readonly GoogleCloudStorageConfigOptions _googleCloudStorageConfigOptions = googleCloudStorageConfigOptions.Value;

    [HttpGet]
    [Authorize(Policy = ReviewerRequirement.PolicyName)]
    public async Task<ActionResult<IEnumerable<ImageResponse>>> GetPendingReviews([FromQuery] int take)
    {
        // Get pending reviews which were initiated by the currently requesting user.
        var imagesUnderUsersReview = await GetClaimedImagesWithPendingReview(take);

        // Get more profile pictures to be reviewed in case there's space for some more.
        var remaining = take - imagesUnderUsersReview.Count();
        var freshImages = await GetUnclaimedImages(remaining);

        // Combine the images to review retrieved based on different criteria.
        var imagesToReview = imagesUnderUsersReview.Concat(freshImages);

        // Prepare response DTO
        var response = imagesToReview.Select(async image => new ImageResponse(
            image.Id,
            await _cloudStorageService.GetSignedUrlAsync(image.FileName, _googleCloudStorageConfigOptions.SignedUrlValidityInSeconds),
            image.UploadDateTime,
            image.Person.FirstName,
            image.Person.LastName,
            image.Person.Cohort?.UniqueName,
            image.Person.Cohort?.DisplayName,
            image.ProfilePictureRequest.ValidationFlags
        ));

        var result = await Task.WhenAll(response);

        return Ok(result);
    }

    [HttpPost]
    [Authorize(Policy = ReviewerRequirement.PolicyName)]
    public async Task<IActionResult> AddReviews(IEnumerable<CreateReviewRequestDTO> reviewDTOs)
    {
        // Retrieve information about the reviewing user from the authorized request
        var reviewerUserId = GetUserId();

        if (reviewerUserId.Equals(Guid.Empty))
        {
            return BadRequest("The reviewers id could not be retrieved from the provided token.");
        }

        // Create review entities
        var reviews = await _reviewService.GenerateReviewEntitiesAsync(reviewDTOs, reviewerUserId, null);
        await _dbContext.ProfilePictureReviews.AddRangeAsync(reviews);

        // Create export entities
        await CreateExportsAsync(reviews);

        // Generate and persist follow-up requests for rejected profile pictures.
        var followUpRequests = await _reviewService.GenerateFollowUpRequestsAsync(reviewDTOs);
        if (followUpRequests.Any())
        {
            // Persist the follow-up requests in the database
            await _dbContext.ProfilePictureRequests.AddRangeAsync(followUpRequests);

            // Generate email notifications
            var notificationRequests = followUpRequests.Select(
                request => _emailNotificationService.GetEmailNotificationRequest(request)
            );

            try
            {
                await _emailNotificationService.SendEmailNotificationRequestsAsync(notificationRequests);
            }
            catch (Exception exception)
            {
                return BadRequest($"Could not send follow-up request as email notification: " + exception.Message);
            }
        }

        // Persist changes
        await _dbContext.SaveChangesAsync();

        return NoContent();
    }

    private async Task<IEnumerable<ProfilePicture>> GetClaimedImagesWithPendingReview(int count)
    {
        var userId = GetUserId();
        var query = _dbContext.ProfilePictures.Where(profilePicture => profilePicture.ReviewClaim.ReviewerId == userId && profilePicture.Review == null);
        return await IncludeNavigationProperties(query)
            .OrderBy(profilePicture => profilePicture.ReviewClaim.ClaimDateTime)
            .Take(count)
            .ToListAsync();
    }

    private async Task<IEnumerable<ProfilePicture>> GetUnclaimedImages(int count)
    {
        if (count <= 0) return [];

        // Get fresh, unclaimed profile pictures to review
        var query = _dbContext.ProfilePictures.Where(profilePicture => profilePicture.ReviewClaim == null && profilePicture.Review == null);
        var unclaimedImages = await IncludeNavigationProperties(query)
            .OrderBy(profilePicture => profilePicture.UploadDateTime)
            .Take(count)
            .ToListAsync();

        // Generate review claim for every unclaimed image
        var reviewClaims = new List<ReviewClaim>();
        var userId = GetUserId();
        var claimDateTime = DateTime.Now;
        foreach (var unclaimedImage in unclaimedImages)
        {
            reviewClaims.Add(new ReviewClaim
            {
                ProfilePictureId = unclaimedImage.Id,
                ReviewerId = userId,
                ClaimDateTime = claimDateTime
            });
        }

        // Add review claims to database
        await _dbContext.ReviewClaims.AddRangeAsync(reviewClaims);
        await _dbContext.SaveChangesAsync();

        return unclaimedImages;
    }

    private Microsoft.EntityFrameworkCore.Query.IIncludableQueryable<ProfilePicture, Cohort?> IncludeNavigationProperties(IQueryable<ProfilePicture> queryable)
    {
        return queryable.Include(profilePicture => profilePicture.ProfilePictureRequest)
                        .ThenInclude(request => request.ValidationFlags)
                        .Include(profilePicture => profilePicture.ProfilePictureRequest)
                        .ThenInclude(request => request.Person)
                        .ThenInclude(person => person.Cohort);
    }

    private Guid GetUserId()
    {
        var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
        return string.IsNullOrWhiteSpace(userId) ? Guid.Empty : Guid.Parse(userId);
    }

    private async Task CreateExportsAsync(IEnumerable<ProfilePictureReview> reviews)
    {
        var pendingExports = reviews
            .Where(review => review.ReviewState == EReviewState.Accepted)
            .Select(review => new PendingExport { ProfilePictureId = review.ProfilePictureId });

        await _dbContext.PendingExports.AddRangeAsync(pendingExports);
    }
}