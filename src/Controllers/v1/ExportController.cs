using Microsoft.AspNetCore.Mvc;
using ProfilePictureDataStore.Services;

namespace ProfilePictureDataStore.Controllers.v1;

[ApiController]
[Route("v1/[controller]")]
public class ExportController(IProfilePictureExportService _exportService, IEmailNotificationService _emailNotificationService) : ControllerBase
{
    [HttpPost]
    public async Task<IActionResult> TriggerPendingExports()
    {
        var exportedProfilePictures = await _exportService.ProcessPendingExportsAsync();

        if (exportedProfilePictures.Any())
        {
            var emailNotificationRequestsTask = exportedProfilePictures.Select(async picture => await _emailNotificationService.GetEmailNotificationRequestAsync(picture));
            var emailNotificationRequests = await Task.WhenAll(emailNotificationRequestsTask);
            await _emailNotificationService.SendEmailNotificationRequestsAsync(emailNotificationRequests);
        }

        return NoContent();
    }
}